require( './db' );
/* Variables */
const _fs             = require( 'fs' );
const _path           = require( 'path' );
const _http           = require( 'http'  );
const _https          = require( 'https' );
global.VALUE_STRING   = 'string';
global.VALUE_OBJECT   = 'object';
global.VALUE_ARRAY    = 'array';
global.VALUE_FUNCTION = 'function';
global.VALUE_FILE     = 'file';
global.VALUE_FOLDER   = 'folder';
global.VALUE_RENAME   = 'rename';
global.VALUE_DELETE   = 'delete';
global.VALUE_CHANGE   = 'change';
global.VALUE_ASYNC    = 'async';
global.VALUE_NUMBER   = 'number';
global.VALUE_BOOLEAN  = 'boolean';
global.VALUE_SCRIPT   = 'script';
global.VALUE_JSON     = 'json';
//###################################################################################################################################################

/* Generales */
function ParseURL( url ) {
	const result = {}, path = [], params = {};
	let  regex, rex;

	url = /^(https?):\/\/(([^\s\/\?\:]*)(\:([0-9]+))?)((\/([^\s\/\?]+)){1,})?(\?([^\s]+))?/g.exec( url );

	result.protocol = url[1];
	result.host     = url[2];
	result.port     = url[5] || '';
	result.params   = url[10] || '';
	result.path     = url[6] || '';

	/* Path */
	regex = /\/([^\s\/]+)/g;

	while ( rex = regex.exec( result.path ) )
		rex[1] && path.push( rex[1] );

	result.separate_path = path;

	/* Parametros */
	regex = /([^\s\=\&]+)\=([^\s\=\&]+)/gm;

	while ( rex = regex.exec( result.params ) )
		params[rex[1]] = rex[2];

	result.separate_params = params;

	result.path+= '?' + result.params;

	return result;
}

global.Tipode = ( type ) => {
	const type_string = Object.prototype.toString.call( type );

	return type_string.substring( 8, type_string.length - 1 ).toLowerCase();
};
global.Mime = ( type ) => {
	if ( Tipode( type )===VALUE_STRING ) {
		switch ( type.toLowerCase() ) {
			case '.js'   : case 'js'   : return 'text/javascript';
			case '.json' : case 'json' : return 'application/json';
			case '.css'  : case 'css'  : return 'text/css';
			case '.ico'  : case 'ico'  : return 'image/x-icon';
			case '.jpg'  : case 'jpg'  : return 'image/jpeg';
			case '.png'  : case 'png'  : return 'image/png';
			case '.svg'  : case 'svg'  : return 'image/svg+xml';
			case 'string':
			case '.txt'  : case 'txt'  : return 'text/plain';
			case '.woff' : case 'woff' : return 'font/woff';
			case '.woff2': case 'woff2': return 'font/woff2';
			case '.pdf'  : case 'pdf'  : return 'application/pdf';
			case '.htm'  : case 'htm'  :
			case '.html' : case 'html' : return 'text/html';
		}
	}

	return 'text/txt; null';
};
global.HttpCode = ( code ) => {
	switch ( code ) {
		case 100: return 'Continue';
		case 101: return 'Switching Protocols';
		case 102: return 'Processing';
		case 103: return 'Early Hints';

		case 200: return 'Ok';
		case 201: return 'Created';
		case 202: return 'Accepted';
		case 203: return 'Non-Authoritative Information';
		case 204: return '';
		case 205: return 'Reset Content';
		case 206: return 'Partial Content';
		case 207: return 'Multi-Status';
		case 208: return 'Already Reported';
		case 226: return 'IM Used';

		case 300: return 'Multiple Choice';
		case 301: return 'Moved Permanently';
		case 302: return 'Found';
		case 303: return 'See Other';
		case 304: return 'Not Modified';
		case 305: return 'Use Proxy';
		case 306: return 'Switch Proxy';
		case 307: return 'Temporary Redirect';
		case 308: return 'Permanent Redirect';

		case 400: return 'Bad Request';
		case 401: return 'Unauthorized';
		case 402: return 'Payment Required';
		case 403: return 'Forbidden';
		case 404: return 'Not Found';
		case 405: return 'Method Not Allowed';
		case 406: return 'Not Acceptable';
		case 407: return 'Proxy Authentication Required';
		case 408: return 'Request Timeout';
		case 409: return 'Conflict';
		case 410: return 'Gone';
		case 411: return 'Length Required';
		case 412: return 'Precondition Failed';
		case 413: return 'Payload Too Large';
		case 414: return 'URI Too Long';
		case 415: return 'Unsupported Media Type';
		case 416: return 'Range Not Satisfiable';
		case 417: return 'Expectation Failed';
		case 418: return 'I\'m a teapot';
		case 421: return 'Misdirected Request';
		case 422: return 'Unprocessable Entity';
		case 423: return 'Locked';
		case 424: return 'Failed Dependency';
		case 426: return 'Upgrade Required';
		case 428: return 'Precondition Required';
		case 429: return 'Too Many Requests';
		case 431: return 'Request Header Fields Too Large';
		case 451: return 'Unavailable For Legal Reasons';

		case 500: return 'Internal Server Error';
		case 501: return 'Not Implemented';
		case 502: return 'Bad Gateway';
		case 503: return 'Service Unavailable';
		case 504: return 'Gateway Timeout';
		case 505: return 'HTTP Version Not Supported';
		case 506: return 'Variant Also Negotiates';
		case 507: return 'Insufficient Storage';
		case 508: return 'Loop Detected';
		case 510: return 'Not Extended';
		case 511: return 'Network Authentication Required';
	}

	return null;
};
//###################################################################################################################################################

/* Funciones - ARCHIVOS */
global.IsExist = ( path ) => {
	try  { return _fs.statSync( path ) }
	catch{ return false                }
};
global.IsFile = ( path ) => {
	try  { return _fs.statSync( path ).isFile() }
	catch{ return false                         }
};
global.IsDir = ( path ) => {
	try  { return _fs.statSync( path ).isDirectory() }
	catch{ return false                              }
};
global.ParsePath = ( ...paths ) => {
	paths = paths.map( path => {
		if ( !path ) return'.';
		else         return path.replace( /~/g, process.env.HOME );
	} );

	let pat        = paths.length===0 ? '.' : ( paths.length===1 ? paths[0] : _path.join( ...paths ) );
	let result     = _path.parse( _path.resolve( pat ) );
	result.path    = _path.join( result.dir, result.base );
	let stat       = IsExist( result.path );
	result.type    = stat ? ( stat.isFile() ? VALUE_FILE : ( stat.isDirectory() ? VALUE_FOLDER : null ) ) : null;
	result.low_ext = result.ext.toLowerCase();
	result.Read    = () => result.data = FileRead( result.path );
	result.Write   = ( data, options ) => { DirCreate( result.dir ); FileWrite( result.path, data, options ) };

	return result;
};

global.DirCreate = ( path ) => {
	if ( global.IsExist( path ) ) return;

	global.DirCreate( global.ParsePath( path ).dir );
	_fs.mkdirSync( path );
};
global.DirTravel = ( path, func, options=null ) => {
	const stat = IsExist( path );
	const check_archive = _file => {
		if ( options ) {
			let view = false;

			if ( options.filter ) view = options.filter.some( regex => { regex.lastIndex = 0; return regex.test( _file ) } );
			else                  view = true;

			if ( view ) {
				if ( options.ignore ) view = !options.ignore.some( regex => { regex.lastIndex = 0; return regex.test( _file ) } );
				else                  view = true;
			}

			return view;
		}

		return true;
	};

	if      ( !stat || typeof func!==VALUE_FUNCTION ) return;
	else if (  stat.isFile()                        ) check_archive( path ) && func( path );
	else if (  stat.isDirectory()                   ) {
		_fs.readdirSync( path ).forEach( file => {
			const path_file = ParsePath( path, file );

			if ( path_file.type===VALUE_FOLDER && ( options?.recursive===undefined || options.recursive ) ) {
				DirTravel( path_file.path, func, options );
			}
			else check_archive( path_file.path ) && func( path_file );
		} );
	}
};

global.FileRead = ( path, isText=true ) => {
	let file;

	try   { file = _fs.readFileSync( path, isText ? 'UTF8' : undefined ) }
	catch { return null                                                  }

	if      ( isText==='lower' ) return file.toLowerCase();
	else if ( isText==='upper' ) return file.toUpperCase();

	return file;
};
global.FileWrite = ( path, data, options='UTF8' ) => {
	try  { _fs.writeFileSync( path, data, options ); return true }
	catch{ return false                                          }
};
global.FileWatch = ( path, callback, options ) => {
	if ( process.platform==='linux' ) {
		if ( Tipode( options )===VALUE_OBJECT && options.recursive ) {
			delete options.recursive;

			( function fpa( _path ) {
				const fil = ParsePath( _path );

				if ( fil.type===VALUE_FOLDER ) {
					FileTravel( _path ).forEach( f => fpa( f.path ) );
					FileWatch( _path, callback, options );
				}
				else if ( fil.type===VALUE_FILE ) FileWatch( path, callback, options );
			} )( path );
		}
		else _fs.watch( path, options, ( eventType, filename ) => {
			const fil     = ParsePath( path, filename );
			fil.eventType = eventType===VALUE_RENAME ? ( fil.type===null ? VALUE_DELETE : VALUE_CHANGE ) : eventType;

			callback(fil);
		} );
	}
	else _fs.watch( path, options, ( eventType, filename ) => {
		const fil     = ParsePath( path, filename );
		fil.eventType = eventType===VALUE_RENAME ? ( fil.type===null ? VALUE_DELETE : VALUE_CHANGE ) : eventType;

		callback( fil );
	} );
};
//###################################################################################################################################################

/* Funciones - AJAX */
global.fetch = function( url, props ) { return new Promise( ( run, err ) => {
	props = { ...props, ...ParseURL( url ) };

	typeof props.body==='object' && ( props.body = JSON.stringify( props.body ) );

	const options = {
		protocol: props.protocol + ':',
		host    : props.host,
		port    : props.port,
		path    : props.path,
		method  : props.method || 'GET',
		headers : Object.assign(
			{ 'Content-Type':'application/x-www-form-urlencoded' },
		 	props.body ? { 'Content-Length':Buffer.byteLength( props.body ) } : null,
		 	props.headers
		 ),
	};

	const request = ( props.protocol==='http' ? _http : _https ).request( options, response => {
		let body = '';

		response.setEncoding( 'utf8' );
		response.on( 'data', d => body+= d );
		response.on( 'end' , () => {
			if ( response.statusCode<400 ) run( { response:response, headers:response.headers, status:response.statusCode, body } );
			else                           err( { response:response, headers:response.headers, status:response.statusCode, body, error:'error en la consulta' } );
		} );
	 } );

	request.on( 'error', err );
	request.write( props.body || '' );
	request.end();
} ) };
//###################################################################################################################################################

/* Declaraciones */
const services = {};

/* Carga de archivos */
function LoadFiles() {
	DirTravel( __dirname, file => {
		const mod                        = require( file.path );
		try {
			services[mod.url?.toLowerCase()] = new mod;

			delete require.cache[ require.resolve( file.path ) ];
		}
		catch{}
	}, { filter:[ /\.service\.js$/gm ] } );
}
//###################################################################################################################################################

/* Servidor */
function ParseParams( url ) {
	if ( url.indexOf( '?' )===-1 ) return {};

	return url
		.substring( url.indexOf( '?' ) + 1 )
		.split( '&' )
		.reduce( ( result, value ) => {
			let operation = value.split( '=' );

			if ( operation[0] && operation[1] && operation[0]!='' && operation[1]!='' )
				result[operation[0] || ''] = (operation[1] || '');

			return result;
		}, {} );
}
function ParseHeaders( headers ) {
	const result = {};
	const camelize = string => {
		string = string.replace( /^[A-z]/, string[0].toLowerCase() );

		const regex = /[^A-z]/g;
		let   rex, replace_string;

		while ( rex = regex.exec( string ) ) {
			replace_string = string.slice( 0, rex.index ) + ( string[rex.index + 1] && string[rex.index + 1].toUpperCase() || '' ) + string.slice( rex.index + 2 );

			if ( replace_string!==string )
				string = replace_string, regex.lastIndex = rex.index;
		}

		return string;
	}

	for ( let x = 0; x<headers.length; x+= 2 ) result[ camelize( headers[x] ) ] = headers[x + 1];

	return result;
}
function ParseKeyHeaders( headers ) {
	const result = [];

	for ( let x = 0; x<headers.length; x+= 2 )
		result.push( headers[x] );

	return result;
}

function RequestResponseModule( context ) {
	let   uri       = '';
	const exec_func = ( func, func_name ) => {
		let params;

		if ( typeof func_name==='string' && typeof func[func_name]==='function' ) {
			func = func[func_name];

			if ( context.type_body==='json' ) params = [...context.json, context];
			else                              params = [   context.body, context];
		}
		else {
			if ( typeof func[context.method]==='function' ) func = func[context.method];
			else                                            func = func.Request;

			params = [context];
		}

		if ( typeof func==='function' ) return func( ...params );
		else                            return 404;
	};

	for ( let i = 0; i<context.path.length; i++ ) {
		uri      += '/' + context.path[i].toLowerCase();
		const func = services[uri];

		if ( func ) return exec_func( func, context.path[i + 1] );
	}

	return 404;
}
function RequestResponseFile( context ) {
	if ( !context.url || context.url==='/' ) context.url = '/index.html';

	const file = ParsePath( './hostless', context.url );

	if ( file.type!==VALUE_FILE ) return 404;

	return {
		_code: 200,
		_body: FileRead( file.path, false ),
		_type: file.ext,
	};
}

function HttpRequestParse( request, response ) {return new Promise( ( run, err ) => {
	let   url = decodeURIComponent( request.url );
	const result = {
		host       : request.headers.host,
		method     : request.method.toUpperCase(),
		raw_url    : url,
		params     : ParseParams( url ),
		headers    : ParseHeaders( request.rawHeaders ),
		key_headers: ParseKeyHeaders( request.rawHeaders ),
		protocol   : 'http',
	};

	if ( url.includes( '?' ) ) url = url.slice( 0, url.indexOf( '?' ) );

	result.url = url;
	url        = url.split( '/' ).splice( 1 );

	if ( url.length===1 && url[0]==='' ) url = [];

	result.path     = url;
	result.petition = { request, response };

	/*BODY*/
	let body = '';

	request
	.on( 'error', err )
	.on( 'data' , c => { body+= c } )
	.on( 'end'  , () => {
		result.rawBody = body;

		if ( typeof result.headers.contentType==='string' && result.headers.contentType.toLowerCase().includes( 'json' ) ) {
			try{ result.body = JSON.parse( body ) } catch { result.body = {} }

			result.type_body = 'json';
		}
		else {
			result.body      = body;
			result.type_body = 'string';
		}

		run( result );
	} );
} ) }
function HttpRequestResponse( context, result ) {
	if      ( result===undefined       ) result = { code:501 };
	else if ( typeof result==='number' ) {
		let text = HttpCode( result );

		if ( text!==null ) result = { body:text, code:result, type:'string', headers:{} };
		else               result = { body:'' + result, code:200 };
	}
	else if ( Tipode( result )==='object' ) {
		result = {
			code   : result._code || 200,
			type   : result._type || 'json',
			body   : result._body!=undefined ? result._body : result,
			cookie : result._cookie,
			headers: result._headers || {},
		};

		delete result.body._code;
		delete result.body._type;
		delete result.body._body;
		delete result.body._cookie;
		delete result.body._headers;

		if ( Tipode( result.body )==='object' && !Object.keys( result.body ).length )
			result.body = HttpCode( result.code );
	}
	else result = { code:200, type:Tipode( result )==='array' ? 'json' : 'string', headers:{}, body:result };

	if ( Tipode( result.body )==='object' || Tipode( result.body )==='array' )
		result.body = JSON.stringify( result.body );

	context.petition.response.writeHead( result.code,
		Object.assign(
			{
				'Content-Type'                : Mime( result.type ),
				'Access-Control-Allow-Origin' : '*',
				'Access-Control-Allow-Headers': 'Content-Type',
			},
			( result.cookie ? { 'set-cookie':result.cookie } : null ),
			result.headers,
		)
	);

	context.petition.response.end( result.body );
}
function HttpRequest( request, response ) {
	HttpRequestParse( request, response )
	.then( context => {
		context.Response = result => HttpRequestResponse( context, result );

		if ( context.method==='OPTIONS' ) HttpRequestResponse( context, 204 );
		else {
			let result = RequestResponseModule( context );

			if ( result===404 )
				result = RequestResponseFile( context );

			if      ( result===undefined           ) context.Response( 501 );
			else if ( Tipode( result )==='promise' ) {
				result
				.then( context.Response )
				.catch( e => {
					console.error( e );
					context.Response( 500 );
				} );
			}
			else context.Response( result );
		}
	} )
	.catch( e => {
		console.error( e );
		response.writeHead( 500, { 'Content-Type': 'text/plain' } );
		response.end( 'Error interno en el servidor' );
	} );
}

function ConnectServer() {
	let port = process.argv.filter( value => value.includes( 'PORT' ) )[0];
	port     = /PORT=(\d+)$/g.exec( port );
	port     = port ? port[1] : '';
	port     = parseInt( port ) || process.env.PORT || 3001;

	_http.createServer( HttpRequest ).listen( port, () => { console.info( `BFF conectado, ${port}...` ) } );
}
//###################################################################################################################################################

/* Start */
LoadFiles();
ConnectServer();
// FileWatch( __dirname, () => { LoadFiles() }, { recursive:true } );
//###################################################################################################################################################

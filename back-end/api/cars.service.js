const model = require( '../db.schemas' );

module.exports = class {
	static url = '/cars';

	async GET() {
		try {
			return ( await model.find() ).map( v => {
				return {
					description  : v.description,
					estimatedate : v.estimatedate,
					id           : v.id,
					image        : v.image,
					make         : v.make,
					model        : v.model,
					in_maintainer: !!v.in_maintainer,
				};
			} );
		}
		catch( e ) {
			console.error( e );
			return 500;
		}
	}
	async POST( context ) {
		try {
			let res = await model.findOneAndUpdate( { id:context.body.id }, { in_maintainer:context.body.in_maintainer } );

			if ( !res ) return 304;

			return 201;
		}
		catch( e ) {
			console.error( e );

			return 500;
		}
	}
}

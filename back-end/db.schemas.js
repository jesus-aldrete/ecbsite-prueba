const mongoose   = require( 'mongoose' );
const CarsSchema = mongoose.Schema( {
	description  : { type:String },
	estimatedate : { type:String },
	id           : { type:Number },
	image        : { type:String },
	make         : { type:String },
	model        : { type:String },
	in_maintainer: { type:Boolean },
}, { collection: 'Cars' } );

module.exports = mongoose.model( 'Cars', CarsSchema );

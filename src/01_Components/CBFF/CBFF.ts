const url:string = 'http://localhost:3001';

export type tCar = {
	description: string,
	estimatedate: string,
	id: number,
	image: string,
	km: number,
	make: string,
	model: string,
	in_maintainer: boolean,
};

export async function GetCars(): Promise<tCar[]> {
	let request:any = await fetch( `${url}/cars` );
	let is_json:boolean = false;

	request.headers.forEach( ( value:string, key:string ) => { if ( key==='content-type' ) is_json = value.toLowerCase().includes( 'json' ) } );

	if ( is_json ) request = await request.json();
	else           request = null;

	if ( request ) return request;

	return [];
}


export async function SetCarsMaintainer( id:number, in_maintainer:boolean ): Promise<void> {
	await fetch( `${url}/cars`, {
		method : 'POST',
		mode: 'cors',
		body   : JSON.stringify( { id, in_maintainer } ),
		headers: {
			'Content-Type': 'application/json',
		},
	} );
}

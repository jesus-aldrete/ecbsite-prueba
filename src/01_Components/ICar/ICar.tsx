import React from 'react';
import { tCar } from '../CBFF/CBFF';
import './ICar.css';

interface iProps {
	data   : tCar,
	onClick: ( event:any ) => void,
}

export default class ICar extends React.Component<iProps> {
	onClick = () => {
		( this as any ).Trigger( 'onSelectData', this.props.data );
	};

	render() {
		return <div className="ICar" onClick={ this.onClick }>
			<img className="image" src={this.props.data.image} alt=""/>
			<div className="cont">
				<div className="name">{this.props.data.make}&nbsp;{this.props.data.model}</div>
				<div className="text">{this.props.data.description}</div>
			</div>
		</div>;
	}
}

import React from 'react';
import { tCar } from '../CBFF/CBFF';
import ICar from '../ICar/ICar';
import './CCarList.css';

interface iProps {
	data: tCar[],
}

export default class CCarList extends React.Component<iProps> {
	onClick = ( event:any ) : void => {
		console.log(222, event);
	}

	render() {
		return <div className="CCarList">
			{ this.props.data?.map( ( car, i ) => <ICar key={i} data={car} onClick={ this.onClick }/> ) }
		</div>;
	}
}

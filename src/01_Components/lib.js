import React from 'react';

/* Storage */
let   _ids     = 0;
const _events = {};

/* Funciones */
function Hash( data ) {
	if ( !data || !data.charCodeAt ) data = ( new Date() ).toString() + ++_ids;

	let hash = 5381, num = ( data || '' ).length;

	while ( num ) hash = ( hash * 33 ) ^ data.charCodeAt( --num );

	return hash >>> 0;
}
//###################################################################################################################################################

/* Extenciones */
Object.defineProperty( React.Component.prototype, 'Subscribe', { enumerable:false, writable:false, value:function( event, callback ) {
	let hash = Hash( event + ( typeof callback==='function' ? ( this[callback.name.replace(/^bound\s*/g, '')]?.toString() || callback?.name ) : '' ) );
	_events[hash] = {
		event,
		callback: _event => {
			if ( typeof callback==='function' ) {
				callback( ..._event._payload??[] )
			}
		},
	};

	return this;
} } );
Object.defineProperty( React.Component.prototype, 'unsubscribe', { enumerable:false, writable:false, value:function( event, callback ) {
	let hash = Hash( event + ( typeof callback==='function' ? this[callback.name.replace(/^bound\s*/g, '')].toString() : '' ) );

	delete _events[hash];

	return this;
} } );
Object.defineProperty( React.Component.prototype, 'Trigger', { enumerable:false, writable:false, value:function( ...params ) {
	const event = params.splice( 0, 1 )[0];

	if ( typeof event==='string' && event!=='' ) {
		const evt = document.createEvent( 'HTMLEvents' );

		evt.initEvent( event, true, true );

		evt._payload = params;

		for ( let key in _events ) {
			if ( _events[key].event===event ) {
				_events[key].callback( evt );
			}
		}
	}

	return this;
} } );
//###################################################################################################################################################

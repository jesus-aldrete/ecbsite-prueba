import React from 'react';
import { tCar, SetCarsMaintainer } from '../CBFF/CBFF';
import './CCarInfo.css';

interface iProps {
	data:tCar,
}
interface iState {
	data:tCar | null,
	in_maintainer: boolean | null,
}

export default class CCarInfo extends React.Component<iProps, iState> {
	constructor( props:iProps ) {
		super( props );

		this.state = {
			data         : null,
			in_maintainer: null,
		};

		(this as any).Subscribe( 'onSelectData', ( data:tCar ) => { this.setState( { data, in_maintainer:data.in_maintainer } ) } );
	}

	onClick = async () => {
		let data           = ( this.state.data || this.props.data );
		data.in_maintainer = !data.in_maintainer;

		(this as any).Trigger( 'onMaintainer', data );
		this.setState( { in_maintainer:data.in_maintainer } );
		await SetCarsMaintainer( data.id, data.in_maintainer );
	};

	render() {
		return <div className={ 'CCarInfo' + ( (this.state.data || this.props.data)?.in_maintainer ? ' in_maintainer' : '' ) }>
			<figure className="image">
				<img src={(this.state.data || this.props.data)?.image} alt=""/>
			</figure>
			<div className="data id          ">{(this.state.data || this.props.data)?.id          }</div>
			<div className="data estimatedate">{(this.state.data || this.props.data)?.estimatedate}</div>
			<div className="data make        ">{(this.state.data || this.props.data)?.make        }</div>
			<div className="data model       ">{(this.state.data || this.props.data)?.model       }</div>
			<div className="data km          ">{(this.state.data || this.props.data)?.km          }</div>
			<div className="data description ">{(this.state.data || this.props.data)?.description }</div>
			<button className="maintainer" onClick={ this.onClick }>{ (this.state.data || this.props.data)?.in_maintainer ? 'Mantenimiento Terminado' : 'Poner en Mantenimiento' }</button>
		</div>;
	}
}

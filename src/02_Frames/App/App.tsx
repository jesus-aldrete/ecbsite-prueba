import React from 'react';
import pattern from '../../03_Resources/pattern.svg';
import FCars from '../../02_Frames/FCars/FCars';
import './App.css';


export default class App extends React.Component {
	render() {
		return <div className="App">
			<img src={pattern} className="pattern" alt="pattern"/>
			<FCars/>
		</div>
	}
}

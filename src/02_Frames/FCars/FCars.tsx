import React from 'react';
import CCarList from '../../01_Components/CCarList/CCarList';
import CCarInfo from '../../01_Components/CCarInfo/CCarInfo';
import { tCar, GetCars } from '../../01_Components/CBFF/CBFF';
import './FCars.css';

interface iProps {}

interface iState {
	data: tCar[],
	info: tCar | null,
}

export default class FCars extends React.Component<iProps, iState> {
	constructor( props:iProps ) {
		super( props );

		this.state = {
			data: [],
			info: null,
		};
	}

	async componentDidMount() {
		this.setState( { data:await GetCars() } );
	}

	render() {
		return <div className="FCars">
			<CCarList data={this.state.data}/>
			<CCarInfo data={this.state.info || this.state.data[0]}/>
		</div>
	}
}

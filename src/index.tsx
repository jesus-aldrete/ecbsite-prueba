import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './02_Frames/App/App';
import './01_Components/lib';

ReactDOM.render(
	<React.StrictMode>
		<App />
	</React.StrictMode>,
	document.getElementById('root')
);
